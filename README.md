# Logging-LQDN

Configuration des fichiers de log à travers journald, logrotate et auditd.

Sur chaque machine, on considère trois niveaux de log :

- Les logs du Kernel
- Les logs du système & services de base ( connexion SSH, commandes shell )
- Les logs des applications

Ce rôle se charger de plusieurs choses ;
- Il va configurer chaque service ( JournalD, LogRotate, AuditD ) sur les machines
    - Les logs serons envoyés sur une machine adminitratrice pour l'analyse
    - Ils ne seront gardé qu'un temps donné, et anonymisé selon les besoins
    - Ils aurons une configuration spécifique selon le(s) service(s) en route


Requirements
------------

N/A

Role Variables
--------------

### JournalD

### LogRotate / Rsyslog

### AuditD

|   Variable    | Valeur par défaut  | Valeurs possibles| Commentaires  |
|---------------|--------------------|------------------|---------------|  
| retention     | 4 ( semaines )       | -1 =< x < 52     |               |
| admin_server         | exemple.fr | IPv4 or IPv6 |                  |               |
| sysadmin_email | example@example.com | Email |    |


Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: admin
      roles:
        - logging-lqdn

    - hosts: server
      roles:
        - logging-lqdn

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
